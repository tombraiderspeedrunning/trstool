#
#  trstool
#  Copyright (C) 2016 DarenK
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

PREFIX = /usr/local

CURDIR = $(shell pwd)
BUILDDIR = $(CURDIR)/build
SRCDIR = $(CURDIR)/src

_OBJ_BIN = \
trstool.o \
libtrs/utils.o \
libtrs/comp.o \
libtrs/ext/vt/l_common.o \
libtrs/ext/vt/l_main.o \
libtrs/ext/vt/l_tr1.o \
libtrs/ext/vt/l_tr2.o \
libtrs/ext/vt/l_tr3.o \
libtrs/ext/vt/l_tr4.o \
libtrs/ext/vt/l_tr5.o \
libtrs/ext/vt/scaler.o \
libtrs/ext/vt/vt_level.o

OBJ_BIN = $(patsubst %,$(BUILDDIR)/obj/%,$(_OBJ_BIN))

CC = gcc
CXX = g++

CFLAGS_std = -Wundef -Wall -Wextra -std=c99 -fPIC
CXXFLAGS_std = -fPIC
OPTFLAGS ?= -O$(OPTLEVEL)

CFLAGS += $(CFLAGS_std) $(CFLAGS_cfg) $(OPTFLAGS) $(IFLAGS) \
		$(shell pkg-config --cflags sdl2) \
		$(shell pkg-config --cflags zlib)

CXXFLAGS += $(CXXFLAGS_std) $(CFLAGS_cfg) $(OPTFLAGS) $(IFLAGS) \
			$(shell pkg-config --cflags sdl2) \
			$(shell pkg-config --cflags zlib)

LDFLAGS += $(LDFLAGS_cfg) \
		$(shell pkg-config --libs sdl2) \
		$(shell pkg-config --libs zlib)

all: prepare $(BUILDDIR)/bin/trstool

$(BUILDDIR)/bin/trstool: $(OBJ_BIN)
	@echo -e "LINKER\t$@"
	$(CXX) -o $@ $(OBJ_BIN) $(CFLAGS) $(LDFLAGS)

$(BUILDDIR)/obj/%.o: $(SRCDIR)/%.cpp
	@printf "CXX\t$@\n"
	$(CXX) -o $@ -c $(CXXFLAGS) $<

$(BUILDDIR)/obj/%.o: $(SRCDIR)/%.c
	@printf "CC\t$@\n"
	$(CC) -o $@ -c $(CFLAGS) $<

prepare:
	@mkdir -p \
	$(BUILDDIR)/bin \
	$(BUILDDIR)/obj/libtrs/ext/vt

install: $(BUILDDIR)/bin/trstool
	install -D $< ${PREFIX}/bin/trstool

uninstall:
	rm -f ${PREFIX}/bin/trstool

.PHONY: clean

clean:
	rm -rf $(BUILDDIR)
