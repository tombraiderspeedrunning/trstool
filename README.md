# trstool
A tool for investigating classic Tomb Raider 1-5.

Join the Tomb Raider Speedruns community:
https://discord.gg/KV6brta

Dependencies:

	sdl2 zlib

Compile and install for Linux or MSYS2:

	$ make
	# make install

Usage:

	# In-Game Time dump for Tomb Raider 1-3 savegames
	# Tomb Raider 1 unfortunately only keeps track of the current level or the
	# previous level when saved at the save prompt at the end of a level (PS1)
	trstool -d tr1-3_savegame

	# Compare data directories of different versions of the game (example TR2)
	trstool --compare-tr2-data data1=./data_original,data2=./data_botched

	# Linux only (playing Tomb Raider games with Wine)
	trstool --read-igt 
	trstool --read-larainfo
	trstool --set-ong
	trstool --set-pre-qwop
	trstool --set-qwop
