/*
 *  Copyright (C) 2017-2019 DarenK
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <inttypes.h>
#include <getopt.h>

// Options
enum trs_opts {
	TRS_DUMP_SAVEGAME,
	TRS_READ_IGT,
	TRS_READ_LARAINFO,
	TRS_SET_ONG,
	TRS_SET_PRE_QWOP,
	TRS_SET_QWOP,
	TRS_COMPARE_TR1_DATA,
	TRS_COMPARE_TR2_DATA,
	TRS_COMPARE_TR3_DATA,
	TRS_COMPARE_TR4_DATA,
	TRS_COMPARE_TR5_DATA
};

// Long options
static struct option trs_opts_long[] = {
	{"d",                 required_argument, 0, TRS_DUMP_SAVEGAME},
	{"dump-savegame",     required_argument, 0, TRS_DUMP_SAVEGAME},
	{"compare-tr1-data",  required_argument, 0, TRS_COMPARE_TR1_DATA},
	{"compare-tr2-data",  required_argument, 0, TRS_COMPARE_TR2_DATA},
	{"compare-tr3-data",  required_argument, 0, TRS_COMPARE_TR3_DATA},
	{"compare-tr4-data",  required_argument, 0, TRS_COMPARE_TR4_DATA},
	{"compare-tr5-data",  required_argument, 0, TRS_COMPARE_TR5_DATA},
#ifdef __linux__
	{"read-igt",          optional_argument, 0, TRS_READ_IGT},
	{"read-larainfo",     optional_argument, 0, TRS_READ_LARAINFO},
	{"set-ong",           required_argument, 0, TRS_SET_ONG},
	{"set-pre-qwop",      optional_argument, 0, TRS_SET_PRE_QWOP},
	{"set-qwop",          optional_argument, 0, TRS_SET_QWOP},
#endif
	{0, 0, 0, 0}
};

// --compare-tr*-data
enum {
	DATA1_OPT,
	DATA2_OPT
};

char *const token_data[] = {
	[DATA1_OPT]   = "data1",
	[DATA2_OPT]   = "data2",
	NULL
};

