/*
 *  Copyright (C) 2017-2019 DarenK
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#define COMPARE_TR_ROOM(DATA) \
	if ((l1.rooms[i].num_##DATA != 0) && (l1.rooms[i].num_##DATA == l2.rooms[i].num_##DATA)) { \
		if (memcmp(l1.rooms[i].DATA, l2.rooms[i].DATA, l1.rooms[i].num_##DATA*sizeof(*l1.rooms[i].DATA))) { \
			printf("\tDifference found in room #%d " #DATA "\n", i); \
		} \
	}

#ifdef __cplusplus
extern "C" {
#endif

int trs_compare_data_directories(char *data1, char *data2, int game_id, char *ext);

#ifdef __cplusplus
}
#endif
