/*
 *  Copyright (C) 2017-2019 DarenK
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined __linux__ || defined __CYGWIN__
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#ifdef __linux__
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <limits.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

#include "utils.h"

const char *tr_exe_names[] = {
	[TR2_ID] = "tomb2.exe",
	[TR3_ID] = "tomb3.exe",
	NULL
};

#define SAVE_SIZE_TR1_PC 10675
#define SAVE_SIZE_TR1_PS 8192
#define SAVE_SIZE_TR2_PC 7442
#define SAVE_SIZE_TR2_PS 8192
#define SAVE_SIZE_TR3_PC 13913
#define SAVE_SIZE_TR3_PS 16384

unsigned char tr1_ps_signature[] = {
	0x53, 0x43, 0x11, 0x01, 0x82, 0x73, 0x82, 0x8f, 0x82, 0x8d, 0x82, 0x82, 0x81, 0x40, 0x82, 0x71,
	0x82, 0x81, 0x82, 0x89, 0x82, 0x84, 0x82, 0x85, 0x82, 0x92, 0x00
};

unsigned char tr2_ps_signature[] = {
	0x53, 0x43, 0x11, 0x01, 0x82, 0x73, 0x82, 0x8f, 0x82, 0x8d, 0x82, 0x82, 0x81, 0x40, 0x82, 0x71,
	0x82, 0x81, 0x82, 0x89, 0x82, 0x84, 0x82, 0x85, 0x82, 0x92, 0x81, 0x40, 0x82, 0x68, 0x82, 0x68,
	0x00
};

unsigned char tr3_ps_signature[] = {
	0x53, 0x43, 0x11, 0x02, 0x82, 0x73, 0x82, 0x8f, 0x82, 0x8d, 0x82, 0x82, 0x81, 0x40, 0x82, 0x71,
	0x82, 0x81, 0x82, 0x89, 0x82, 0x84, 0x82, 0x85, 0x82, 0x92, 0x81, 0x40, 0x82, 0x68, 0x82, 0x68,
	0x82, 0x68, 0x00
};

const char *tr2_levelnames[] = {
	"The Great Wall",
	"Venice",
	"Bartoli's Hideout",
	"Opera House",
	"Offshore Rig",
	"Diving Area",
	"40 Fathoms",
	"Wreck of the Maria Doria",
	"Living Quarters",
	"The Deck",
	"Tibetan Foothills",
	"Barkhang Monastery",
	"Catacombs of the Talion",
	"Ice Palace",
	"Temple of Xian",
	"Floating Islands",
	"The Dragon's Lair",
	"Home Sweet Home",
	"Total In-Game Time"
};

const char *tr3_levelnames[] = {
	"Jungle",
	"Temple Ruins",
	"The River Ganges",
	"Caves of Kaliya",
	"Coastal Village",
	"Crash Site",
	"Madubu Gorge",
	"Temple of Puna",
	"Thames Wharf",
	"Aldwych",
	"Lud's Gate",
	"City",
	"Nevada Desert",
	"High Security Compound",
	"Area 51",
	"Antarctica",
	"RX-Tech Mines",
	"Lost City of Tinnos",
	"Meteorite Cavern",
	"Total In-Game Time"
};

const char *separator = "======================================= ";

// Offsets to the Levelstats
#define OFFSET_LS_TR2_PC 0x7b
#define OFFSET_LS_TR2_PS 0x22c

enum {
	TR2_SECRET_GOLD = 0x1,
	TR2_SECRET_JADE = 0x2,
	TR2_SECRET_STONE = 0x4
};

enum {
	TR2_WEAPON_UNARMED = 0x1,
	TR2_WEAPON_PISTOLS = 0x2,
	TR2_WEAPON_AP = 0x4,
	TR2_WEAPON_UZI = 0x8,
	TR2_WEAPON_SG = 0x10,
	TR2_WEAPON_M16 = 0x20,
	TR2_WEAPON_GL = 0x40,
	TR2_WEAPON_HG = 0x80
};


#pragma pack(push, 1)
struct levelstats_tr2 {
	uint16_t health;
	uint16_t ap_ammo;
	uint16_t uzi_ammo;
	uint16_t sg_ammo;
	uint16_t m16_ammo;
	uint16_t gl_ammo;
	uint16_t hg_ammo;
	uint8_t small_med_count;
	uint8_t large_med_count;
	uint8_t bytes_1;
	uint8_t flare_count;
	uint8_t bytes_2[2];
	uint8_t weapon_flag;
	uint8_t bytes_[3];
	uint32_t timer;
	uint32_t ammo_used;
	uint32_t hits;
	uint32_t distance_travelled;
	uint16_t kills;
	uint8_t secrets;
	uint8_t meds_used; // 1 = single medipack, 2 = large medipack
};
#pragma pack(pop)

#ifdef __linux__
#pragma pack(push, 1)
struct larainfo_tr2 {
	int32_t ground;			// 0x00
	uint32_t interaction;	// 0x04
	uint16_t visibility;	// 0x08
	uint8_t gap_1[2];		// 0x0a
	uint16_t id;			// 0x0c
	uint16_t action;		// 0x0e
	uint16_t next_action;	// 0x10
	uint8_t gap_2[4];		// 0x12
	uint8_t anim_frame;		// 0x13
	uint8_t animation;		// 0x14
	uint16_t room;			// 0x16
	int16_t last_entity;
	uint8_t gap_3[2];
	int16_t hspeed;
	int16_t vspeed;
	int16_t health;
	uint8_t gap_4[16];
	int32_t x;
	int32_t y;
	int32_t z;
	uint16_t pitch;
	uint16_t yaw;
	uint16_t roll;
	uint16_t flags;
};
#pragma pack(pop)
#endif

#ifdef __linux__
#pragma pack(push, 1)
struct larainfo_tr3 {
	int32_t yPosGround;
	int32_t interaction;
	int32_t meshBits; // <-- this one is 32 bits
	int16_t entityID;
	int16_t currentAction;
	int16_t nextAction;
	int16_t requiredAction; // <-- required end action
	int16_t animNumber; // <-- index for animation array
	uint8_t animFrame;
	uint8_t currAnim;
	int16_t currRoomNum;
	int16_t LastActiveEntityIndex;
	int16_t nextActive;
	int16_t hspeed;
	int16_t vspeed;
	int16_t health;
	int16_t tileRelated;
	int16_t timer;
	int16_t stateFlags;
	int16_t shade; // dynamic lighting has 0xFFFF value
	//void* data;
	uint8_t stuff[20];
	int32_t x;
	int32_t y;
	int32_t z;
	uint16_t pitch;
	uint16_t yaw;
	uint16_t roll;
	uint16_t flags; // isActive|pad|pad|gravity|bulletHit|collidable|hasBeenSeen
};
#pragma pack(pop)
#endif

/*==========================================
 * Checks for Tomb Raider 1-3 savegames
 * return values:
 * 0: none found
 * TR1_PC: Tomb Raider 1 PC
 * TR1_PS: Tomb Raider 1 PlayStation
 * TR2_PC: Tomb Raider 2 PC
 * TR2_PS: Tomb Raider 2 PlayStation
 * TR3_PC: Tomb Raider 3 PC
 * TR3_PS: Tomb Raider 3 PlayStation
 *
 * also sets the offset of the block
 *------------------------------------------*/
int trs_find_savegame_signature(char *file, size_t *offset)
{
	FILE *fp;
	size_t filesize;
	char *filebuffer;

	fp = fopen(file, "rb");

	if (!fp) {
		printf("Can't open file \"%s\"", file);
		return 0;
	}

	fseek(fp, 0, SEEK_END);
	filesize = ftell(fp);
	rewind(fp);

	if (filesize == SAVE_SIZE_TR1_PC) {
		*offset = 0;
		printf("Seems to be a TR1 PC savegame from filesize\n");
		return TR1_PC;
	} else if (filesize == SAVE_SIZE_TR2_PC) {
		*offset = 0;
		printf("Seems to be a TR2 PC savegame from filesize\n");
		return TR2_PC;
	} else if (filesize == SAVE_SIZE_TR3_PC) {
		*offset = 0;
		printf("Seems to be a TR3 PC savegame from filesize\n");
		return TR3_PC;
	}

	filebuffer = (char*)malloc(filesize+1);

	if (fread(filebuffer, 1, filesize, fp) != filesize) goto read_err;

	filebuffer[filesize] = '\0';

	for (int i = 0; i < (int)filesize; i++) {
		if (!strcmp(&filebuffer[i], (char*)tr1_ps_signature)) {
			printf("Found TR1 PS1 savegame signature in offset 0x%x\n", i);
			*offset = i;
			return TR1_PS;
		} else if (!strcmp(&filebuffer[i], (char*)tr2_ps_signature)) {
			printf("Found TR2 PS1 savegame signature in offset 0x%x\n", i);
			*offset = i;
			return TR2_PS;
		} else if (!strcmp(&filebuffer[i], (char*)tr3_ps_signature)) {
			printf("Found TR3 PS1 savegame signature in offset 0x%x\n", i);
			*offset = i;
			return TR3_PS;
		}
	}

	free(filebuffer);
	fclose(fp);

	printf("Couldn't find any TR2/3 savegames in file \"%s\"\n", file);

	return 0;
read_err:
	printf("Read error in file \"%s\"\n", file);
	free(filebuffer);
	fclose(fp);
	return 0;
}


/*==========================================
 * Parses the ingame times of Tomb Raider 1-3
 * savegames
 *------------------------------------------*/
int trs_dump_savegame(char *file)
{
	FILE *fp;
	unsigned char savegame[16384 + 1];

	int tr_id;
	size_t offset;

	char time_string[10];
	unsigned int time_ticks;
	unsigned int total_ticks = 0;

	int num_levels = 0;
	unsigned int first_offset = 0, to_next_offset = 0;
	size_t savegame_size;
	const char **levelnames = NULL;

	tr_id = trs_find_savegame_signature(file, &offset);

	switch (tr_id) {
		case TR1_PC:
			first_offset = 0x199;
			savegame_size = SAVE_SIZE_TR1_PC;
			break;
		case TR1_PS:
			first_offset = 0x334;
			savegame_size = SAVE_SIZE_TR1_PS;
			break;
		case TR2_PC:
			num_levels = 18;
			first_offset = 0x93;
			to_next_offset = 0x28;
			levelnames = tr2_levelnames;
			savegame_size = SAVE_SIZE_TR2_PC;
			break;
		case TR2_PS:
			num_levels = 18;
			first_offset = 0x244;
			to_next_offset = 0x28;
			levelnames = tr2_levelnames;
			savegame_size = SAVE_SIZE_TR2_PS;
			break;
		case TR3_PC:
			num_levels = 19;
			first_offset = 0xf5;
			to_next_offset = 0x2f;
			levelnames = tr3_levelnames;
			savegame_size = SAVE_SIZE_TR3_PC;
			break;
		case TR3_PS:
			num_levels = 19;
			first_offset = 0x2b4;
			to_next_offset = 0x30;
			levelnames = tr3_levelnames;
			savegame_size = SAVE_SIZE_TR3_PS;
			break;
		default:
			printf("Invalid savegame provided\n");
			return -1;
	}

	if (!(fp = fopen(file, "rb"))) {
		printf("Can't open file '%s'", file);
		return 0;
	}

	// Read Savegame into memory
	fseek(fp, offset, SEEK_SET);
	memset(savegame, 0, 16384 + 1);
	fread(savegame, 1, savegame_size, fp);

	fseek(fp, offset, SEEK_SET);

	// TR1 only keeps the time of the current level or the last finished level when
	// saving at the end-of-level save prompt (PS1)
	if (tr_id == TR1_PC || tr_id == TR1_PS) {
		fseek(fp, first_offset, SEEK_CUR);

		if (fread(&time_ticks, 4, 1, fp) != 1) goto read_err;

		trs_get_time_string_from_ticks(time_ticks, time_string);

		printf("Times for %s:\n%s\n", file, separator);

		printf("%s - Time of current or finished level\n", time_string);
	} else if (tr_id == TR2_PC || tr_id == TR2_PS) {
		// Point levelstats_tr2_ps pointer to offset
		struct levelstats_tr2 *ls = (void*)&savegame[tr_id&TR2_PC ? OFFSET_LS_TR2_PC : OFFSET_LS_TR2_PS];
		char secrets[5];

		secrets[4] = '\0';

		for (int i = 0; i < num_levels; i++) {
			printf("%s\n%s\n%s\n", separator, tr2_levelnames[i], separator);

			trs_get_time_string_from_ticks(ls[i].timer, time_string);
			printf("Time Taken: %s (%d frames)\n", time_string, ls[i].timer);

			memset(secrets, 0x20, 4);
			if (ls[i].secrets) {
				if (ls[i].secrets&TR2_SECRET_GOLD)  secrets[0] = 'G';
				if (ls[i].secrets&TR2_SECRET_JADE)  secrets[1] = 'J';
				if (ls[i].secrets&TR2_SECRET_STONE) secrets[2] = 'S';
				printf("Secrets Found: %s\n", secrets);
			} else {
				printf("Secrets Found: None\n");
			}

			printf("Kills: %d\n", ls[i].kills);
			printf("Ammo used: %d\n", ls[i].ammo_used);
			printf("Hits: %d\n", ls[i].hits);
			printf("Health Packs Used: %g\n", ls[i].meds_used*0.5);

			if ((double)ls[i].distance_travelled/445 > 1000.0) {
				printf("Distance Travelled: %.2lfkm\n", (double)ls[i].distance_travelled/(445*1000));
			} else {
				printf("Distance Travelled: %.lfm\n", (double)ls[i].distance_travelled/445);
			}

			printf("%s\n", separator);

			printf("\n%s%s\nItems for the Next Level\n%s%s\n", separator, separator, separator, separator);

			//printf("Health: %d\n", ls[i].health);
			printf("Starting Ammo: ");

			if (ls[i].weapon_flag&TR2_WEAPON_SG) {
				printf("SG: ");
			} else {
				printf("(SG): ");
			}
			printf("%d ", ls[i].sg_ammo/6);

			if (ls[i].weapon_flag&TR2_WEAPON_AP) {
				printf("AP: ");
			} else {
				printf("(AP): ");
			}
			printf("%d ", ls[i].ap_ammo);

			if (ls[i].weapon_flag&TR2_WEAPON_UZI) {
				printf("UZI: ");
			} else {
				printf("(UZI): ");
			}
			printf("%d ", ls[i].uzi_ammo);

			if (ls[i].weapon_flag&TR2_WEAPON_M16) {
				printf("M16: ");
			} else {
				printf("(M16): ");
			}
			printf("%d ", ls[i].m16_ammo);

			if (ls[i].weapon_flag&TR2_WEAPON_GL) {
				printf("GL: ");
			} else {
				printf("(GL): ");
			}
			printf("%d ", ls[i].gl_ammo);

			if (ls[i].weapon_flag&TR2_WEAPON_HG) {
				printf("HG: ");
			} else {
				printf("(HG): ");
			}
			printf("%d ", ls[i].hg_ammo);
			printf("\n");

			printf("Small Medipacks: %d ", ls[i].small_med_count);
			printf("Large Medipacks: %d ", ls[i].large_med_count);
			printf("Flares: %d\n", ls[i].flare_count);

			printf("%s%s\n\n", separator, separator);
		}

		// Final statistics
		{
			struct levelstats_tr2 ls_sum;

			memset(&ls_sum, 0, sizeof(struct levelstats_tr2));

			for (int i = 0; i < num_levels; i++) {
				ls_sum.timer += ls[i].timer;

				ls_sum.secrets += ls[i].secrets&TR2_SECRET_GOLD ? 1 : 0;
				ls_sum.secrets += ls[i].secrets&TR2_SECRET_JADE ? 1 : 0;
				ls_sum.secrets += ls[i].secrets&TR2_SECRET_STONE ? 1 : 0;

				ls_sum.kills += ls[i].kills;

				ls_sum.ammo_used += ls[i].ammo_used;

				ls_sum.hits += ls[i].hits;

				ls_sum.meds_used += ls[i].meds_used;

				ls_sum.distance_travelled += ls[i].distance_travelled;
			}

			printf("%s\nFinal Statistics\n%s\n", separator, separator);

			trs_get_time_string_from_ticks(ls_sum.timer, time_string);
			printf("Time Taken: %s (%d frames)\n", time_string, ls_sum.timer);

			printf("Secrets Found: %d of 48\n", ls_sum.secrets);

			printf("Kills: %d\n", ls_sum.kills);
			printf("Ammo used: %d\n", ls_sum.ammo_used);
			printf("Hits: %d\n", ls_sum.hits);
			printf("Health Packs Used: %g\n", ls_sum.meds_used*0.5);

			if ((double)ls_sum.distance_travelled/445 > 1000.0) {
				printf("Distance Travelled: %.2lfkm\n", (double)ls_sum.distance_travelled/(445*1000));
			} else {
				printf("Distance Travelled: %.lfm\n", (double)ls_sum.distance_travelled/445);
			}

			printf("%s\n\n", separator);
		}

		// Overview of In-Game Times
		printf("Times for %s:\n%s\n", file, separator);
		for (int i = 0; i < num_levels; i++) {
			trs_get_time_string_from_ticks(ls[i].timer, time_string);
			printf("%s - %s\n", time_string, levelnames[i]);
			total_ticks += ls[i].timer;
		}

		trs_get_time_string_from_ticks(total_ticks, time_string);

		printf("%s\n", separator);
		printf("%s - Total In-Game Time\n", time_string);
	} else if (tr_id == TR3_PC || tr_id == TR3_PS) {
		printf("Times for %s:\n%s\n", file, separator);

		for (int i = 0; i < num_levels; i++) {
			// Time is measured in ticks equal to 1/30th of a second in an uint32
			if (i < num_levels) {
				if (i == 0) {
					fseek(fp, first_offset, SEEK_CUR);
				} else {
					fseek(fp, to_next_offset, SEEK_CUR);
				}

				if (fread(&time_ticks, 4, 1, fp) != 1) goto read_err;

				trs_get_time_string_from_ticks(time_ticks, time_string);
			}

			printf("%s - %s\n", time_string, levelnames[i]);
		}

		// Total Time
		trs_get_time_string_from_ticks(total_ticks, time_string);

		printf("%s\n", separator);
		printf("%s - Total In-Game Time\n", time_string);
	}
	fclose(fp);
	return 1;

read_err:
	printf("Read error in file '%s'\n", file);
	fclose(fp);
	return 0;
}

void trs_get_time_string_from_ticks(uint32_t time_ticks, char *time_string)
{
	uint32_t time_hours, time_minutes, time_seconds, time_milliseconds;

	time_hours = (time_ticks/(30*60*60)) % 100; // 99h max
	time_minutes = (time_ticks/(30*60)) % 60;
	time_seconds = (time_ticks/30) % 60;
	time_milliseconds = (time_ticks % 30)*(1000/30.0) + 0.5;

	sprintf(time_string,
			"%02d:%02d:%02d.%03d",
			time_hours,
			time_minutes,
			time_seconds,
			time_milliseconds);

	return;
}

#ifdef __linux__
int trs_get_tr_pid(int *pid, enum tr_id *tid)
{
	char status_file[256];
	char pid_name[256];

	FILE *fp;

	DIR *d;
	struct dirent *dir;

	printf("Looking for a Tomb Raider process...");

	d = opendir("/proc");
	if (d) {
		while ((dir = readdir(d)) != NULL) {
			if ((*pid = atoi(dir->d_name))) {
				int i = 0;
				sprintf(status_file, "/proc/%d/status", *pid);

				if (!(fp = fopen(status_file, "r"))) {
					continue;
				}

				fscanf(fp, "Name:\t%s\n", pid_name);
				fclose(fp);

				while (tr_exe_names[i]) {
					if (strstr(pid_name, tr_exe_names[i])) {
						printf(" Found process %s with pid %d!\n", pid_name, *pid);
						*tid = i;
						closedir(d);
						return 0;
					}
					i++;
				}
			}
		}

		closedir(d);
	}

	printf(" No Tomb Raider process found.\n");

	return -1;
}

/*
 * IGT
 */
int trs_read_igt(char *split_key)
{
	int pid;
	enum tr_id tid;

	while (1) {
		if (!trs_get_tr_pid(&pid, &tid)) {
			switch (tid) {
				case TR2_ID:
					printf("\n");
					for (int i = 3; i > 0; i--) {
						printf("Reading TR2 IGT from pid %d in %ds...\r", pid, i);
						fflush(stdout);
						sleep(1);
					}
					trs_read_igt_tr2(pid, split_key);
					break;
				case TR3_ID:
					printf("Found TR3 process, but IGT reader is not implemented\n");
					break;
			}
		}
		sleep(1);
	}

	return 0;
}

int trs_read_igt_tr2(int pid, char *split_key)
{
	int mem_fd;
	char mem_file_name[256];
	struct stat stat_fd;

	char time_string[10];
	uint32_t total_time = 0;
	uint32_t total_time_previous = 0;

	uint8_t current_level = 0;

	uint64_t offset_levelstats = 0x00400000 + 0x0011ea2c;
	uint64_t offset_levelstats_current_level = 0x00400000 + 0x0011ee08;

	struct levelstats_tr2 ls[18];
	struct levelstats_tr2 ls_c;

	char *split_command;

	char igt_overview[4096];

	if (split_key) {
		split_command = malloc(strlen("xdotool key ") + strlen(split_key) + strlen(" --delay 500") + 1);

		strcpy(split_command, "xdotool key ");
		strcat(split_command, split_key);
	}

	sprintf(mem_file_name, "/proc/%d/mem", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);

	if ((mem_fd = open(mem_file_name, O_RDONLY)) < 0) {
		fprintf(stderr, "Can't open mem_file '%s', no permissions?\n", mem_file_name);
		return 2;
	}

	while (!fstat(mem_fd, &stat_fd) && stat_fd.st_nlink > 0) {
		lseek(mem_fd, offset_levelstats, SEEK_SET);
		if (read(mem_fd, &ls, 18*sizeof(struct levelstats_tr2)) <= 0) { // Levelstats all levels
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}
		lseek(mem_fd, offset_levelstats_current_level, SEEK_SET);
		if (read(mem_fd, &ls_c, sizeof(struct levelstats_tr2)) <= 0) { // Levelstats current level
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}

		for (int i = 0; i < 19; i++) {
			if (ls[i].timer == 0) {
				ls[i].timer = ls_c.timer;

				if (current_level != i + 1) { // On level change
					current_level = i + 1;
					if (split_key) {
						system(split_command);
					}
				}

				break;
			}
		}

		memset(igt_overview, 0, 4096);
		igt_overview[0] = '\n';

		total_time_previous = total_time;
		total_time = 0;

		for (int i = 0; i < current_level; i++) {
			trs_get_time_string_from_ticks(ls[i].timer, time_string);

			sprintf(&igt_overview[strlen(igt_overview)],
					"%s - %s\n",
					time_string,
					tr2_levelnames[i]);

			if (i == current_level - 1) {
				for (int j = 0; j < 18; j++) {
					total_time += ls[j].timer;
				}

				trs_get_time_string_from_ticks(total_time, time_string);
				sprintf(&igt_overview[strlen(igt_overview)],
						"%s - %s\n",
						time_string,
						tr2_levelnames[18]);
			}
		}

		printf("%s", igt_overview);
		fflush(stdout);

		if (split_key && !total_time_previous && total_time) { // New Game, split
			system(split_command);
		}

		usleep(33333);
	}

	printf("TR2 seems to have exited\n");
	close(mem_fd);
	kill(pid, SIGSTOP);
	waitpid(pid, NULL, 0);
	ptrace(PTRACE_DETACH, pid, NULL, NULL);

	return 0;
}

/*
 * Larainfo
 */
int trs_read_larainfo(char *logfile)
{
	int pid;
	enum tr_id tid;

	while (1) {
		if (!trs_get_tr_pid(&pid, &tid)) {
			switch (tid) {
				case TR2_ID:
					trs_read_larainfo_tr2(pid, logfile);
					break;
				case TR3_ID:
					trs_read_larainfo_tr3(pid, logfile);
					break;
			}
		}

		sleep(1);
	}

	return 0;
}

int trs_read_larainfo_tr2(int pid, char *logfile)
{
	int mem_fd;
	char mem_file_name[256];
	struct stat stat_fd;

	struct larainfo_tr2 li, li_old;
	uint64_t offset_larapointer = 0x5207dc;
	uint32_t offset_larainfo;

	FILE *fp_log = NULL;

	sprintf(mem_file_name, "/proc/%d/mem", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);

	if ((mem_fd = open(mem_file_name, O_RDONLY)) < 0) {
		fprintf(stderr, "Can't open mem_file '%s', no permissions?\n", mem_file_name);
		return -1;
	}

	if (logfile && !(fp_log = fopen(logfile, "w"))) {
		fprintf(stderr, "Cannot write to logfile '%s'.\n", logfile);
		return -1;
	}

	while (!fstat(mem_fd, &stat_fd) && stat_fd.st_nlink > 0) {
		lseek(mem_fd, offset_larapointer, SEEK_SET);
		if (read(mem_fd, &offset_larainfo, sizeof(int32_t)) <= 0) {
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}

		memcpy(&li_old, &li, sizeof(struct larainfo_tr2));

		lseek(mem_fd, offset_larainfo, SEEK_SET);
		if (read(mem_fd, &li, sizeof(struct larainfo_tr2)) <= 0) {
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}

		printf("X = %.0lf:%d (%d)\n", (double)li.x/1024, li.x % 1024, li.x);
		printf("Y = %.0lf:%d (%d)\n", (double)li.z/1024, li.z % 1024, li.z);
		printf("Z = %.0lf:%d (%d)\n", -(double)li.y/1024, -li.y % 1024, -li.y);
		printf("Angle = %.3lf (%d)\n", (double)li.yaw/0x10000*360, li.yaw);
		printf("Speed horizontal = %d units/frame (real: %.2lf)\n", li.hspeed, sqrt(pow(li_old.x - li.x, 2) + pow(li_old.z - li.z, 2)));
		printf("Speed vertical   = %d units/frame\n", -li.vspeed);
		printf("Health = %.1lf%%\n", (double)li.health/10);
		printf("Room ID = %d\n", li.room);
		printf("Animation Frame = %d\n", li.anim_frame);
		printf("Animation = %d\n", li.animation);
		printf("Air = %d\n", li.flags);

		if (-li.vspeed < 0 && !li.flags && li.animation < 7) {
			printf("Pre-QWOP = ACTIVE (%d)\n", li.flags);
		} else {
			printf("Pre-QWOP = inactive (%d)\n", li.flags);
		}

		if (fp_log) {
			fprintf(fp_log, "%d %d %d %d\n", li.x, li.z, -li.y, -li.vspeed);
		}

		usleep(33333);
	}

	printf("TR2 seems to have exited\n");
	close(mem_fd);
	kill(pid, SIGSTOP);
	waitpid(pid, NULL, 0);
	ptrace(PTRACE_DETACH, pid, NULL, NULL);

	return 4;
}

int trs_read_larainfo_tr3(int pid, char *logfile)
{
	int mem_fd;
	char mem_file_name[256];
	struct stat stat_fd;

	struct larainfo_tr3 li, li_old;
	uint64_t offset_larapointer = 0x6d62a4;
	uint32_t offset_larainfo;

	FILE *fp_log = NULL;

	sprintf(mem_file_name, "/proc/%d/mem", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);

	if ((mem_fd = open(mem_file_name, O_RDONLY)) < 0) {
		fprintf(stderr, "Can't open mem_file '%s', no permissions?\n", mem_file_name);
		return -1;
	}

	if (logfile && !(fp_log = fopen(logfile, "w"))) {
		fprintf(stderr, "Cannot write to logfile '%s'.\n", logfile);
		return -1;
	}

	while (!fstat(mem_fd, &stat_fd) && stat_fd.st_nlink > 0) {
		lseek(mem_fd, offset_larapointer, SEEK_SET);
		if (read(mem_fd, &offset_larainfo, sizeof(int32_t)) <= 0) {
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}

		memcpy(&li_old, &li, sizeof(struct larainfo_tr3));

		lseek(mem_fd, offset_larainfo, SEEK_SET);
		if (read(mem_fd, &li, sizeof(struct larainfo_tr3)) <= 0) {
			ptrace(PTRACE_DETACH, pid, NULL, NULL);
			close(mem_fd);
		}

		printf("X = %.0lf:%d (%d)\n", (double)li.x/1024, li.x % 1024, li.x);
		printf("Y = %.0lf:%d (%d)\n", (double)li.z/1024, li.z % 1024, li.z);
		printf("Z = %.0lf:%d (%d)\n", -(double)li.y/1024, -li.y % 1024, -li.y);
		printf("Angle = %.3lf (%d)\n", (double)li.yaw/0x10000*360, li.yaw);
		printf("Speed horizontal = %d units/frame (real: %.2lf)\n", li.hspeed, sqrt(pow(li_old.x - li.x, 2) + pow(li_old.z - li.z, 2)));
		printf("Speed vertical   = %d units/frame\n", -li.vspeed);
		printf("Health = %.1lf%%\n", (double)li.health/10);
		printf("Room ID = %d\n", li.currRoomNum);
		printf("Animation Frame = %d\n", li.animFrame);
		printf("Animation = %d\n", li.animNumber);
		printf("Air = %d\n", li.flags);
		if (-li.vspeed < 0 && !li.flags && li.animNumber < 7) {
			printf("Pre-QWOP = ACTIVE (%d)\n", li.flags);
		} else {
			printf("Pre-QWOP = inactive (%d)\n", li.flags);
		}

		if (fp_log) {
			fprintf(fp_log, "%d %d %d %d\n", li.x, li.z, -li.y, -li.vspeed);
		}

		usleep(33333);
	}

	printf("TR3 seems to have exited\n");
	close(mem_fd);
	kill(pid, SIGSTOP);
	waitpid(pid, NULL, 0);
	ptrace(PTRACE_DETACH, pid, NULL, NULL);

	return 4;
}


/*
 * State
 */
int trs_set_state(int state)
{
	int pid;
	enum tr_id tid;

	if (!trs_get_tr_pid(&pid, &tid)) {
		switch (tid) {
			case TR2_ID:
				trs_set_state_tr2(pid, state);
				break;
			case TR3_ID:
				trs_set_state_tr3(pid, state);
				break;
		}
	}

	return 0;
}

int trs_set_state_tr2(int pid, int state)
{
	int mem_fd;
	char mem_file_name[256];

	struct larainfo_tr2 li, li_old;
	uint64_t offset_larapointer = 0x5207dc;
	uint32_t offset_larainfo;

	sprintf(mem_file_name, "/proc/%d/mem", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);

	if ((mem_fd = open(mem_file_name, O_RDWR)) < 0) {
		fprintf(stderr, "Can't open mem_file '%s', no permissions?\n", mem_file_name);
		return -1;
	}

	// Read larapointer, then read larainfo
	lseek(mem_fd, offset_larapointer, SEEK_SET);
	if (read(mem_fd, &offset_larainfo, sizeof(int32_t)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}
	memcpy(&li_old, &li, sizeof(struct larainfo_tr2));
	lseek(mem_fd, offset_larainfo, SEEK_SET);
	if (read(mem_fd, &li, sizeof(struct larainfo_tr2)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}

	// Manipulate stuff
	switch (state) {
		case TRS_STATE_PRE_QWOP:
			li.vspeed = 37;
			break;
		case TRS_STATE_PRE_QWOP_POS1:
			li.x = li.x - li.x % 1024 + 468;
			li.z = li.z - li.z % 1024 + 923;
			li.yaw = 13876;
			break;
		case TRS_STATE_PRE_QWOP_POS2:
		case TRS_STATE_PRE_QWOP_POS3:
		case TRS_STATE_PRE_QWOP_POS4:
		case TRS_STATE_PRE_QWOP_POS5:
		case TRS_STATE_PRE_QWOP_POS6:
		case TRS_STATE_PRE_QWOP_POS7:
		case TRS_STATE_PRE_QWOP_POS8:
			printf("Pre-QWOP 2-8 -> TODO\n");
			break;
		case TRS_STATE_QWOP:
			li.flags = 8;
			break;
		case TRS_STATE_ONG_1:
			// right side at 270° angle
			li.x = li.x - li.x % 1024 + 923;
			li.z = li.z - li.z % 1024 + 550;
			li.yaw = 56336;
			break;
		case TRS_STATE_ONG_2:
			li.x = li.x - li.x % 1024 + 923;
			li.z = li.z - li.z % 1024 + 500;
			li.yaw = 6*UINT16_MAX/4 - 56336;
			break;
		case TRS_STATE_ONG_3:
			li.x = li.x - li.x % 1024 + 101;
			li.z = li.z - li.z % 1024 + 500;
			li.yaw = 56336 - UINT16_MAX/2;
			break;
		case TRS_STATE_ONG_4:
		case TRS_STATE_ONG_5:
		case TRS_STATE_ONG_6:
		case TRS_STATE_ONG_7:
		case TRS_STATE_ONG_8:
			printf("Old New Glitch state 4-8 -> TODO\n");
			break;
	};

	// Write larainfo back to memory
	lseek(mem_fd, offset_larainfo, SEEK_SET);
	if (write(mem_fd, &li, sizeof(struct larainfo_tr2)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}

	return 4;
}

int trs_set_state_tr3(int pid, int state)
{
	int mem_fd;
	char mem_file_name[256];

	struct larainfo_tr3 li, li_old;
	uint64_t offset_larapointer = 0x6d62a4;
	uint32_t offset_larainfo;
	sprintf(mem_file_name, "/proc/%d/mem", pid);

	ptrace(PTRACE_SEIZE, pid, NULL, NULL);

	if ((mem_fd = open(mem_file_name, O_RDWR)) < 0) {
		fprintf(stderr, "Can't open mem_file '%s', no permissions?\n", mem_file_name);
		return -1;
	}

	// Read larapointer, then read larainfo
	lseek(mem_fd, offset_larapointer, SEEK_SET);
	if (read(mem_fd, &offset_larainfo, sizeof(int32_t)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}
	memcpy(&li_old, &li, sizeof(struct larainfo_tr3));
	lseek(mem_fd, offset_larainfo, SEEK_SET);
	if (read(mem_fd, &li, sizeof(struct larainfo_tr3)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}

	// Manipulate stuff
	switch (state) {
		case TRS_STATE_PRE_QWOP:
			li.vspeed = 37;
			break;
		case TRS_STATE_PRE_QWOP_POS1:
			li.x = li.x - li.x % 1024 + 468;
			li.z = li.z - li.z % 1024 + 923;
			li.yaw = 13876;
			break;
	};

	// Write larainfo back to memory
	lseek(mem_fd, offset_larainfo, SEEK_SET);
	if (write(mem_fd, &li, sizeof(struct larainfo_tr3)) <= 0) {
		ptrace(PTRACE_DETACH, pid, NULL, NULL);
		close(mem_fd);
	}

	return 4;
}
#endif
