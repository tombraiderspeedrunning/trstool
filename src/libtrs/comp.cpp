/*
 *  Copyright (C) 2017-2019 DarenK
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <string.h>

#include "ext/vt/l_main.h"

#include "comp.h"

int trs_compare_data_directories(char *data1, char *data2, int game_id, char *ext)
{
	TR_Level l1, l2;

	char file1[256], file2[256];

	struct dirent *entry;
	DIR *dir = opendir(data1);

	char ext_upper[5];

	for (int i = 0; i < 4; i++) {
		ext_upper[i] = toupper(ext[i]);
	}


	if (dir == NULL) {
		printf("Can't open data1 directory '%s'", data1);
		return 1;
	}

	printf("Comparing '%s' level files of data directories '%s' and '%s'...\n",
			ext, data1, data2);
	fflush(stdout);

	while ((entry = readdir(dir)) != NULL) {
		if (!(!strcmp(&entry->d_name[strlen(entry->d_name) - 4], ext)
			|| !strcmp(&entry->d_name[strlen(entry->d_name) - 4], ext_upper))) {
			printf("Entry '%s' doesn't seem to be a Tomb Raider level file, skipping...\n",
					entry->d_name);
			fflush(stdout);
			continue;
		}

		memset(file1, 0, sizeof(file1));
		memset(file2, 0, sizeof(file2));

		strcpy(file1, data1);
		strcpy(file2, data2);

		strcat(file1, "/");
		strcat(file2, "/");

		strcat(file1, entry->d_name);
		strcat(file2, entry->d_name);

		printf("Comparing %s with %s...\n", file1, file2);
		fflush(stdout);

		l1.read_level(file1, game_id);
		l2.read_level(file2, game_id);

		if (l1.rooms_count && (l1.rooms_count == l2.rooms_count)) {
			for (int i = 0; i < l1.rooms_count; i++) {
				COMPARE_TR_ROOM(layers)
				COMPARE_TR_ROOM(lights)
				COMPARE_TR_ROOM(portals)
				//COMPARE_TR_ROOM(sector)
				COMPARE_TR_ROOM(sprites)
				COMPARE_TR_ROOM(static_meshes)
				COMPARE_TR_ROOM(triangles)
				COMPARE_TR_ROOM(rectangles)
				COMPARE_TR_ROOM(vertices)
			}
		} else {
			printf("Room count for both levels is either 0 or different (%d, %d), aborting...\n",
					l1.rooms_count, l2.rooms_count);
			continue;
		}
	}

	closedir(dir);
	
	return 0;
}
