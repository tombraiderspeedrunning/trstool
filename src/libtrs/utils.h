/*
 *  Copyright (C) 2017-2019 DarenK
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <inttypes.h>

// Enum for Memory Card/Savegame dump
enum tr_save {
	TR1_PC = 0x1,
	TR1_PS,
	TR2_PC,
	TR2_PS,
	TR3_PC,
	TR3_PS
};

// Enum for Debugging on PC, should correspond to the offsettr_exe_names
enum tr_id {
	TR2_ID = 0,
	TR3_ID,
};


enum {
	TRS_STATE_PRE_QWOP = 0x0,
	TRS_STATE_PRE_QWOP_POS1 = 0x1,
	TRS_STATE_PRE_QWOP_POS2 = 0x2,
	TRS_STATE_PRE_QWOP_POS3 = 0x3,
	TRS_STATE_PRE_QWOP_POS4 = 0x4,
	TRS_STATE_PRE_QWOP_POS5 = 0x5,
	TRS_STATE_PRE_QWOP_POS6 = 0x6,
	TRS_STATE_PRE_QWOP_POS7 = 0x7,
	TRS_STATE_PRE_QWOP_POS8 = 0x8,

	TRS_STATE_QWOP = 0x10,

	TRS_STATE_ONG = 0x20,
	TRS_STATE_ONG_1 = 0x21,
	TRS_STATE_ONG_2 = 0x22,
	TRS_STATE_ONG_3 = 0x23,
	TRS_STATE_ONG_4 = 0x24,
	TRS_STATE_ONG_5 = 0x25,
	TRS_STATE_ONG_6 = 0x26,
	TRS_STATE_ONG_7 = 0x27,
	TRS_STATE_ONG_8 = 0x28
};

void trs_get_time_string_from_ticks(uint32_t time_ticks, char *time_string);

int trs_find_savegame_signature(char *file, size_t *offset);
int trs_dump_savegame(char *file);

#ifdef __linux__
int trs_get_tr_pid(int *pid, enum tr_id *tid);

int trs_read_igt(char *split_key);
int trs_read_igt_tr2(int pid, char *split_key);

int trs_read_larainfo(char *logfile);
int trs_read_larainfo_tr2(int pid, char *logfile);
int trs_read_larainfo_tr3(int pid, char *logfile);

int trs_set_state(int state);
int trs_set_state_tr2(int pid, int state);
int trs_set_state_tr3(int pid, int state);
#endif
