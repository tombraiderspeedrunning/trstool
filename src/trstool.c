/*
 *  Copyright (C) 2017-2019 DarenK
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined __linux__ || defined __CYGWIN__
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#include "libtrs/trs.h"

#include "trstool.h"

static void show_help(void);

int main(int argc, char *argv[])
{
	int32_t game_id = 127;
	char ext[5] = {0};

	char *data1, *data2;

	int option_index;

	int opt;

	printf("trstool Copyright (C) 2017-2019 DarenK\n\n"
		   "Join the Tomb Raider Speedruns (TRS) Community:\n"
		   "https://discord.gg/KV6brta\n\n");


	if (argc < 2) {
		show_help();
		return 1;
	}

	switch (opt = getopt_long(argc, argv, "", trs_opts_long, &option_index)) {
		case TRS_DUMP_SAVEGAME:
			return trs_dump_savegame(optarg);
		case TRS_COMPARE_TR1_DATA:
		case TRS_COMPARE_TR2_DATA:
		case TRS_COMPARE_TR3_DATA:
		case TRS_COMPARE_TR4_DATA:
		case TRS_COMPARE_TR5_DATA:
			switch (opt) {
				case TRS_COMPARE_TR1_DATA:
					game_id = 0;
					strcpy(ext, ".phd");
					break;
				case TRS_COMPARE_TR2_DATA:
					game_id = 3;
					strcpy(ext, ".tr2");
					break;
				case TRS_COMPARE_TR3_DATA:
					game_id = 5;
					strcpy(ext, ".tr2");
					break;
				case TRS_COMPARE_TR4_DATA:
					game_id = 6;
					strcpy(ext, ".tr4");
					break;
				case TRS_COMPARE_TR5_DATA:
					game_id = 8;
					strcpy(ext, ".tr4");
					break;
			}

			if (!optarg) {
				show_help();
				return EXIT_FAILURE;
			}

			// Sloppy but should work if typed correctly
			data2 = strdup(strchr(strchr(optarg, '=') + 1, '=') + 1);
			*strchr(optarg, ',') = '\0';
			data1 = strdup(strchr(optarg, '=') + 1);

			trs_compare_data_directories(data1, data2, game_id, ext);

			return 0;
#ifdef __linux__
		case TRS_READ_IGT:
			return trs_read_igt(optarg);
		case TRS_READ_LARAINFO:
			return trs_read_larainfo(optarg);
		case TRS_SET_ONG:
			return trs_set_state(TRS_STATE_ONG + atoi(optarg));
		case TRS_SET_PRE_QWOP:
			return trs_set_state(TRS_STATE_PRE_QWOP + optarg ? atoi(optarg) : 0);
		case TRS_SET_QWOP:
			return trs_set_state(TRS_STATE_QWOP);
#endif
		default:
			show_help();
			return EXIT_FAILURE;
	}

	return 0;
}

static void show_help(void)
{
	printf("%s",
		   "Usage:\n"
		   "trstool -d|--dump-savegame <TR savegame>\n"
		   "trstool --compare-tr<1-5>-data data1=<data_directory1>,data2=<data_directory2>\n");
#ifdef __linux__
	printf("%s",
		   "trstool --read-igt <Split Key (optional)>\n"
		   "trstool --read-larainfo <logfile (optional)>\n"
		   "trstool --set-ong X (X = 1-8)\n"
		   "trstool --set-pre-qwop <X (optional)> (X = 1-8 for walk-run-jump positions)\n"
		   "trstool --set-qwop\n");
#endif
}

